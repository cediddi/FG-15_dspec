#!/usr/bin/env python3
import PIL
import PIL.Image
import sys
import os
import glob

SIZE = 64, 48


def get_filenames():
    matched = []
    if len(sys.argv) == 1:
        while not matched:
            inf = input("Please input a file path or a glob pattern: ")
            matched = glob.glob(inf)
    else:
        for path in sys.argv[1:]:
            matched += glob.glob(path)
        if not matched:
            print("No file found with {}".format(sys.argv[1:]))
    return matched


def bitswap(i):
    return int('{:08b}'.format(i)[::-1], 2)


def format_image(infile):
    raw_img = PIL.Image.open(infile)
    raw_img.thumbnail(SIZE, PIL.Image.NONE)
    bw_img = raw_img.convert("1")
    base_dir = os.path.dirname(infile)
    os.makedirs(os.path.join(base_dir, "thumbnail"), exist_ok=True)
    bw_img.save(os.path.join(base_dir, "thumbnail", infile))
    return bw_img


def image_to_buffer(image):
    buffer = bytearray()
    for h_chunk in range(6):
        for w in range(64):
            val = 0
            for bit in range(8):
                val <<= 1
                try:
                    if image.getpixel((w, h_chunk * 8 + bit)):
                        val += 1
                except IndexError:
                    pass
            buffer.append(bitswap(val))
    return buffer


def save_buffer(buffer, infile):
    base_dir = os.path.dirname(infile)
    fname = os.path.splitext(os.path.split(infile)[1])[0]
    os.makedirs(os.path.join(base_dir, "framebuffers"), exist_ok=True)
    with open(os.path.join(base_dir, "framebuffers", fname + ".frb"), "wb") as f:
        f.write(buffer)


if __name__ == "__main__":
    files = get_filenames()
    for f in files:
        save_buffer(image_to_buffer(format_image(f)), f)
