from machine import I2C, Pin
from ssd1306 import SSD1306_I2C
import os
import time

i2c = I2C(scl=Pin(5), sda=Pin(4))
oled_conn = SSD1306_I2C(width=64, height=48, i2c=i2c, addr=0x3c)

while True:
    for filename in os.listdir("/sd/resimler"):
        if filename.endswith(".frb"):
            with open("/sd/resimler/" + filename, "rb") as f:
                oled_conn.write_data(f.read())
                time.sleep(1)
